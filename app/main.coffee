{app, BrowserWindow, globalShortcut, ipcMain, clipboard, Tray, Menu, dialog} = require('electron')

win = null
appIcon = null

show = -> win.show(); win.focus(); win.webContents.send 'focus'
paste = -> win.webContents.send 'paste', clipboard.readText()

addTray = =>
  appIcon = new Tray("#{__dirname}/gfx/favicon.png")
  appIcon.setToolTip 'Vocabus - Dictionary at your fingertips!'
  appIcon.setContextMenu Menu.buildFromTemplate [
    {
      label: 'Show and paste'
      accelerator: 'CmdOrCtrl+Shift+C'
      click: -> show(); paste()
    }
    {
      label: 'Show'
      accelerator: 'CmdOrCtrl+Shift+X'
      click: show
    }
    {
      type: 'separator'
    }
    {
      label: 'Quit'
      role: 'quit'
    }
  ]

createWindow = =>
  win = new BrowserWindow
    width: 800
    height: 600
    minWidth: 800
    minHeight: 600
    skipTaskbar: true
    icon: "#{__dirname}/gfx/logo.png"
    frame: false
  win.loadURL("file://#{__dirname}/home/home.html")
  win.on 'closed', =>
    win = null
    app.quit()

app.on 'ready', =>
  addTray()
  createWindow()
  globalShortcut.register 'CmdOrCtrl+Shift+C', => show(); paste()
  globalShortcut.register 'CmdOrCtrl+Shift+X', => show()
  if app.dock? and app.dock.hide?
    app.dock.hide()

app.on 'will-quit', =>
  globalShortcut.unregisterAll()
  appIcon.destroy()

app.on 'activate', =>
  createWindow() if not win

app.on 'window-all-closed', =>
  app.quit() if process.platform != 'darwin'

ipcMain.on 'save-dialog', (event, options) ->
  type = options.type
  options.defaultPath = "#{app.getPath('desktop')}/#{options.defaultPath}"
  dialog.showSaveDialog win, options, (filename) ->
    event.sender.send('save-dialog-response', {type: type, path: filename})

ipcMain.on 'open-dialog', (event, options) ->
  type = options.type
  options.defaultPath = "#{app.getPath('desktop')}"
  dialog.showOpenDialog win, options, (filename) ->
    event.sender.send('open-dialog-response', {type: type, path: filename})
