fs = require('fs')
{cards} = require('../service/config')
preloader = require('../service/preloader')
dictList = require('../service/dictList')
{ipcRenderer} = require('electron')

$body = $('body')
$body.append(fs.readFileSync(__dirname + '/cards.html', 'utf8'))

$modal = $('#cards-modal')
$table = $('#cards-table')
$cardsDictionary = $('#cards-dictionary')

$body.on 'click', 'a[target=card-add]', ->
  td = $(this).parent()
  tds = td.siblings()
  hash = $(this).parent().data('hash')
  dict = $(this).parent().data('dict')
  [source, target] = [ tds[0].innerHTML, tds[1].innerHTML ]
  [source, target] = [target, source] if $(this).parent().data('reversed')
  cards.add(
    hash: hash
    dict: dict
    source: source
    target: target
  )
  this.outerHTML = '<span class="fa fa-check-circle"></span>';
  return false

$body.on 'click', 'a[target=card-remove]', ->
  return false unless confirm('Do you really want to remove this card?')
  hash = $(this).parent().data('hash')
  $(this).parents('tr')[0].outerHTML = '';
  cards.remove({ hash: hash })
  return false

$modal.on 'shown.bs.modal', ->
  $table.html preloader
  dict = dictList.getCurrent()
  $cardsDictionary.html "#{dictList.getIcon(dict)} #{dictList.getName(dict)}"
  currentCards = cards.findAll({dict: dict})
  unless Object.keys(currentCards).length
    $table.html '<tr><td class="alert alert-warning text-center">No cards saved for this dictionary</td></tr>'
    return false
  out = ''
  for i, translation of currentCards
    out += "<tr class='translation'>
              <td>#{translation.source}</td>
              <td>#{translation.target}</td>
              <td data-hash='#{translation.hash}'>
                <a href='#' target='card-remove'>
                  <span class='fa fa-times-circle'></span>
                </a>
              </td>
            </tr>"
  $table.html out

String::clearCsv = ->
  str = this.replace(/<(?:.|\n)*?>/gm, '').trim().replace("\n",'').replace('"', '\"')
  if str.indexOf(';') > -1 then '"'+str+'"' else str

$body.on 'click', 'a[target=cards-export]', ->
  dict = dictList.getCurrent()
  ipcRenderer.send('save-dialog', {
    type: 'cards'
    title: 'Export your cards',
    filters: [{ name: 'Comma Separated Values', extensions: ['csv'] }]
    defaultPath: "#{dictList.getName(dict, '-')}.csv"
  })
  return false

ipcRenderer.on 'save-dialog-response', (event, msg) ->
  return unless msg.type == 'cards' and msg.path?
  dict = dictList.getCurrent()
  csvData = dictList.getName(undefined, ';') + "\n"
  for i, card of cards.findAll({dict: dict})
    csvData += card.source.clearCsv() + ';' + card.target.clearCsv() + "\n"
  fs.writeFile msg.path, csvData, (err) ->
    alert if err then 'Unexpected error' else 'Cards exported'
