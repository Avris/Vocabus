{dictionaries} = require('../service/config')

languagesName =
  es: 'Spanish'
  en: 'English'
  fr: 'French'
  de: 'German'
  pl: 'Polish'
  sl: 'Slovenian'
  ru: 'Russian'
  it: 'Italian'
  pt: 'Portugese'
  tr: 'Turkish'
  el: 'Greek'
  la: 'Latin'
  lb: 'Elvish'
  zh: 'Chinese'
  nl: 'Dutch'
  da: 'Danish'
  cs: 'Czech'
  hu: 'Hungarian'
  no: 'Norwegian'
  sv: 'Swedish'

class DictList
  constructor: ->
    @reset()
  reset: ->
    @dictionaries = dictionaries.get()
    @current = 0
  getCurrent: ->
    @dictionaries[@current]
  prev: ->
    return if @current == 0
    @current--
  next: ->
    return if @current == @dictionaries.length - 1
    @current++
  set: (i) ->
    @current = i
  count: ->
    @dictionaries.length
  list: ->
    @dictionaries
  getIcon: (dict = @getCurrent()) ->
    "<img src='../gfx/lng/#{dict.substr(0,2)}.png' alt='#{languagesName[dict.substr(0,2)]}' class='lang'/>
     <img src='../gfx/lng/#{dict.substr(2)}.png' alt='#{languagesName[dict.substr(2)]}' class='lang'/>"
  getName: (dict = @getCurrent(), separator = ' «» ') ->
    "#{languagesName[dict.substr(0,2)]}#{separator}#{languagesName[dict.substr(2)]}"
  getLangName: (lng) ->
    languagesName[lng]
  has: (dict) ->
    @dictionaries.indexOf(dict) >= 0
  add: (dict) ->
    dictionaries.add(dict)
  remove: (query) ->
    dictionaries.remove(query)

module.exports = new DictList()
