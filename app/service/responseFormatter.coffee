dictList = require('../service/dictList')
cards = require('../service/config').cards
sha1 = require('sha1')

module.exports =
  format: (data) -> if data then format(data) else '<div class="alert alert-warning text-center">No results found</div>'

format = (data) ->
  out = ''
  $.each data, (i, section) ->
    langReversed = section.lang == dictList.getCurrent().substr(2)
    $.each section.hits, (j, hit) ->
      switch hit.type
        when 'translation' then out += addTranslation(hit, langReversed)
        when 'entry'
          $.each hit.roms, (k, rom) ->
            out += '<tr class="rom"><th>'+rom.headword_full+'</th></tr>'
            $.each rom.arabs, (l, arab) ->
              out += '<tr class="arab"><th>'+arab.header+'</th></tr>' if arab.header
              out += '<tr><td>'
              out += '<table class="table table-hover">'
              $.each arab.translations, (m, translation) ->
                out += addTranslation(translation, langReversed)
              out += '</table>'
              out += '</td></tr>'
        else
          console.log data
  return '<table class="table">'+out+'</table>'

addTranslation = (translation, langReversed) ->
  hash = sha1({
    source: translation.source
    target: translation.target
  })

  return "<tr class='translation'>
            <td>#{translation.source}</td>
            <td>#{translation.target}</td>
            <td data-hash='#{hash}' data-dict='#{dictList.getCurrent()}' data-reversed='#{+langReversed}'>
              #{buildCardButton(hash)}
            </td>
          </tr>"

buildCardButton = (hash) ->
  if cards.find({hash: hash})
    '<span class="fa fa-check-circle"></span>'
  else
    '<a href="#" target="card-add"><span class="fa fa-plus-circle"></span></a>'
