{remote: {app}} = require('electron')
fs = require('fs')
mkdirp = require('mkdirp')

basePath = app.getPath('userData') + '/config/'
mkdirp(basePath)

isArray = Array.isArray || (value) -> return {}.toString.call(value) is '[object Array]'
matches = (value, query) ->
  return value == query if typeof query == 'string'
  for k, v of query
    return false if value[k] != v
  return true

class Config
  constructor: (name, def = []) ->
    @path = basePath + name
    @def = def
  get: ->
    return @value if @value?
    @value = if fs.existsSync(@path) then JSON.parse(fs.readFileSync(@path)) else @def
  set: (value) ->
    @value = value
    fs.writeFile(@path, JSON.stringify(value))
  add: (value) ->
    @get().push(value)
    @set(@value)
  clear: ->
    @set(@def)
  find: (query) ->
    return false unless isArray @get()
    for val, i in @get()
      return {key: i, value: val} if matches(val, query)
    return false
  findAll: (query) ->
    all = {}
    return all unless isArray @get()
    for val, i in @get()
      all[i] = val if matches(val, query)
    return all
  remove: (query) ->
    found = @find(query)
    return false if found == false
    value = @get()
    value.splice(found.key, 1)
    @set(value)
    return true

module.exports =
  aboutShown: new Config('aboutShown', false)
  dictionaries: new Config('dictionaries', ['deen'])
  cards: new Config('cards', [])
