fs = require('fs')
dictList = require('../service/dictList')
preloader = require('../service/preloader')

$body = $('body')
$body.append(fs.readFileSync(__dirname + '/dictionaries.html', 'utf8'))
$dictionariesTable = $('#dictionaries-table')
$dictionariesAdd = $('#dictionaries-add')
$dictionariesList = $('#dictionaries-list')
$dictionariesCurrent = $('#dictionaries-current')
$query = $('#query')
$modal = $('#dictionaries-modal')

availableDictionaries = {}

refresh = (all = true) ->
  if all
    dictList.reset()
    $dictionariesList.html buildDictionariesList()
    $dictionariesTable.html buildDictionariesTable()
    $dictionariesAdd.html buildDictionariesAdd()
  $dictionariesCurrent.html dictList.getIcon()

buildDictionariesList = ->
  out = ''
  $.each dictList.list(), (i, dict) ->
    out += "<li><a href='#' target='dictionary-switch'>#{dictList.getIcon(dict)} #{dictList.getName(dict)}</a></li>"
  out += "<li role='separator' class='divider'></li>
          <li><a href='#dictionaries-modal' target='modal'><span class='fa fa-gears'></span> Configure</a></li>"
  out

buildDictionariesTable = ->
  out = ''
  $.each dictList.list(), (i, dict) ->
    button = "<a href='#' target='dictionary-remove' data-key='#{dict}'><span class='fa fa-times-circle'></span></a>"
    out += "<tr>
              <td>
                <img src='../gfx/lng/#{dict.substr(0,2)}.png' class='lang'/>
                #{dictList.getLangName(dict.substr(0,2))}
              </td>
              <td>
                <img src='../gfx/lng/#{dict.substr(2)}.png' class='lang'/>
                #{dictList.getLangName(dict.substr(2))}
              </td>
              <td>
                #{if dictList.count() > 1 then button}
              </td>
            </tr>"
  out

buildDictionariesAdd = () ->
  "<div class='dropdown'>
    <a role='button' data-toggle='dropdown' class='btn btn-primary'>
        <span class='fa fa-plus-circle'></span> Add dictionary <span class='caret'></span>
    </a>
    <ul class='dropdown-menu multi-level' role='menu'>
      #{buildPrimaryLangs(availableDictionaries)}
    </ul>
  </div>"

buildPrimaryLangs = (availableDictionaries) ->
  out = ''
  for lng, sublangs of availableDictionaries
    out += "<li class='dropdown-submenu'>
              <a href='#' tabindex='-1' target='none'>
                <img src='../gfx/lng/#{lng}.png'/>
                #{dictList.getLangName(lng)}
              </a>
              <ul class='dropdown-menu'>
                #{buildSecondaryLangs(sublangs)}
              </ul>
            </li>"
  out

buildSecondaryLangs = (sublangs) ->
  out = ''
  for lng2, key of sublangs
    continue if dictList.has(key)
    out += "<li>
              <a href='#' target='dictionary-add' data-key='#{key}'>
                <img src='../gfx/lng/#{lng2}.png'/>
                #{dictList.getLangName(lng2)}
              </a>
            </li>"
  out

refresh()

$query.keydown (e) ->
  switch e.keyCode
    when 38 #up
      dictList.prev()
      refresh(false)
      return false
    when 40 #down
      dictList.next()
      refresh(false)
      return false

$modal.on 'shown.bs.modal', ->
  $dictionariesTable.html buildDictionariesTable()
  $dictionariesAdd.html preloader
  $.ajax
    url: "http://api.pons.com/v1/dictionaries?language=en",
    type: "GET",
    success: (data) ->
      addAvailableDictionary = (lng1, lng2, key) ->
        availableDictionaries[lng1] = {} if typeof availableDictionaries[lng1] is "undefined"
        availableDictionaries[lng1][lng2] = key if typeof availableDictionaries[lng1][lng2] is "undefined"
      for i, dictionary of data
        lngs = dictionary.languages
        continue if lngs.length != 2
        addAvailableDictionary lngs[0], lngs[1], dictionary.key
        addAvailableDictionary lngs[1], lngs[0], dictionary.key
      refresh()
  return false

$body.on 'click', 'a[target=dictionary-add]', ->
  dictList.add($(this).data('key'))
  refresh()
  return false

$body.on 'click', 'a[target=dictionary-remove]', ->
  dictList.remove($(this).data('key'))
  refresh()
  return false

$body.on 'click', 'a[target=dictionary-switch]', ->
  dictList.set(parseInt($(this).parent().index()))
  refresh(false)
  $query.click().focus()
  return false
