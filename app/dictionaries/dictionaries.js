// Generated by CoffeeScript 1.10.0
(function() {
  var $body, $dictionariesAdd, $dictionariesCurrent, $dictionariesList, $dictionariesTable, $modal, $query, availableDictionaries, buildDictionariesAdd, buildDictionariesList, buildDictionariesTable, buildPrimaryLangs, buildSecondaryLangs, dictList, fs, preloader, refresh;

  fs = require('fs');

  dictList = require('../service/dictList');

  preloader = require('../service/preloader');

  $body = $('body');

  $body.append(fs.readFileSync(__dirname + '/dictionaries.html', 'utf8'));

  $dictionariesTable = $('#dictionaries-table');

  $dictionariesAdd = $('#dictionaries-add');

  $dictionariesList = $('#dictionaries-list');

  $dictionariesCurrent = $('#dictionaries-current');

  $query = $('#query');

  $modal = $('#dictionaries-modal');

  availableDictionaries = {};

  refresh = function(all) {
    if (all == null) {
      all = true;
    }
    if (all) {
      dictList.reset();
      $dictionariesList.html(buildDictionariesList());
      $dictionariesTable.html(buildDictionariesTable());
      $dictionariesAdd.html(buildDictionariesAdd());
    }
    return $dictionariesCurrent.html(dictList.getIcon());
  };

  buildDictionariesList = function() {
    var out;
    out = '';
    $.each(dictList.list(), function(i, dict) {
      return out += "<li><a href='#' target='dictionary-switch'>" + (dictList.getIcon(dict)) + " " + (dictList.getName(dict)) + "</a></li>";
    });
    out += "<li role='separator' class='divider'></li> <li><a href='#dictionaries-modal' target='modal'><span class='fa fa-gears'></span> Configure</a></li>";
    return out;
  };

  buildDictionariesTable = function() {
    var out;
    out = '';
    $.each(dictList.list(), function(i, dict) {
      var button;
      button = "<a href='#' target='dictionary-remove' data-key='" + dict + "'><span class='fa fa-times-circle'></span></a>";
      return out += "<tr> <td> <img src='../gfx/lng/" + (dict.substr(0, 2)) + ".png' class='lang'/> " + (dictList.getLangName(dict.substr(0, 2))) + " </td> <td> <img src='../gfx/lng/" + (dict.substr(2)) + ".png' class='lang'/> " + (dictList.getLangName(dict.substr(2))) + " </td> <td> " + (dictList.count() > 1 ? button : void 0) + " </td> </tr>";
    });
    return out;
  };

  buildDictionariesAdd = function() {
    return "<div class='dropdown'> <a role='button' data-toggle='dropdown' class='btn btn-primary'> <span class='fa fa-plus-circle'></span> Add dictionary <span class='caret'></span> </a> <ul class='dropdown-menu multi-level' role='menu'> " + (buildPrimaryLangs(availableDictionaries)) + " </ul> </div>";
  };

  buildPrimaryLangs = function(availableDictionaries) {
    var lng, out, sublangs;
    out = '';
    for (lng in availableDictionaries) {
      sublangs = availableDictionaries[lng];
      out += "<li class='dropdown-submenu'> <a href='#' tabindex='-1' target='none'> <img src='../gfx/lng/" + lng + ".png'/> " + (dictList.getLangName(lng)) + " </a> <ul class='dropdown-menu'> " + (buildSecondaryLangs(sublangs)) + " </ul> </li>";
    }
    return out;
  };

  buildSecondaryLangs = function(sublangs) {
    var key, lng2, out;
    out = '';
    for (lng2 in sublangs) {
      key = sublangs[lng2];
      if (dictList.has(key)) {
        continue;
      }
      out += "<li> <a href='#' target='dictionary-add' data-key='" + key + "'> <img src='../gfx/lng/" + lng2 + ".png'/> " + (dictList.getLangName(lng2)) + " </a> </li>";
    }
    return out;
  };

  refresh();

  $query.keydown(function(e) {
    switch (e.keyCode) {
      case 38:
        dictList.prev();
        refresh(false);
        return false;
      case 40:
        dictList.next();
        refresh(false);
        return false;
    }
  });

  $modal.on('shown.bs.modal', function() {
    $dictionariesTable.html(buildDictionariesTable());
    $dictionariesAdd.html(preloader);
    $.ajax({
      url: "http://api.pons.com/v1/dictionaries?language=en",
      type: "GET",
      success: function(data) {
        var addAvailableDictionary, dictionary, i, lngs;
        addAvailableDictionary = function(lng1, lng2, key) {
          if (typeof availableDictionaries[lng1] === "undefined") {
            availableDictionaries[lng1] = {};
          }
          if (typeof availableDictionaries[lng1][lng2] === "undefined") {
            return availableDictionaries[lng1][lng2] = key;
          }
        };
        for (i in data) {
          dictionary = data[i];
          lngs = dictionary.languages;
          if (lngs.length !== 2) {
            continue;
          }
          addAvailableDictionary(lngs[0], lngs[1], dictionary.key);
          addAvailableDictionary(lngs[1], lngs[0], dictionary.key);
        }
        return refresh();
      }
    });
    return false;
  });

  $body.on('click', 'a[target=dictionary-add]', function() {
    dictList.add($(this).data('key'));
    refresh();
    return false;
  });

  $body.on('click', 'a[target=dictionary-remove]', function() {
    dictList.remove($(this).data('key'));
    refresh();
    return false;
  });

  $body.on('click', 'a[target=dictionary-switch]', function() {
    dictList.set(parseInt($(this).parent().index()));
    refresh(false);
    $query.click().focus();
    return false;
  });

}).call(this);
