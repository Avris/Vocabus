{ipcRenderer, remote, shell} = require('electron')
dictList = require('../service/dictList')
responseFormatter = require('../service/responseFormatter')
preloader = require('../service/preloader')

$body = $('body')
$query = $('#query')
$output = $('#output')
$searchBtn = $('#search')

$output.focus()

ipcRenderer.on 'focus', (event) ->
  $query.focus()

ipcRenderer.on 'paste', (event, val) ->
  $query.val(val).focus()
  search()

$(document).on 'keydown', (e) ->
  if e.keyCode == 27
    if $('.modal.in').length
      $('.modal.in').modal('hide')
    else
      remote.getCurrentWindow().hide()

search = ->
  q = $query.focus().val().trim()
  return false unless q
  $(document).scrollTop(0)
  $output.html preloader
  $.ajax
    url: "http://api.pons.com/v1/dictionary?q=#{encodeURIComponent(q)}&l=#{dictList.getCurrent()}"
    type: "GET"
    headers: {'X-Secret': 'b22b39772a239ae6ed1812d2f8445257becdf242716ec6d2f87321e71841e710'}
    success: (data) ->
      $output.html responseFormatter.format(data)

$query.keydown (e) ->
  search() if e.keyCode == 13
$searchBtn.click search

currentCards = []

$body.on 'click', 'a[target=modal]', ->
  $($(this).attr('href')).modal()
  return false

$body.on 'click', 'a[target=external]', ->
  shell.openExternal $(this).attr('href')
  return false

$body.on 'click', 'a[target=quit]', ->
  remote.getCurrentWindow().hide()
  return false

$body.on 'click', 'a[target=none]', ->
  return false
