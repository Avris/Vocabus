fs = require('fs')
config = require('../service/config')
{ipcRenderer} = require('electron')

$body = $('body')
$body.append(fs.readFileSync(__dirname + '/about.html', 'utf8'))

unless config.aboutShown.get()
  config.aboutShown.set(true)
  $('#about-modal').modal()

$body.on 'click', 'a[target=config-export]', ->
  ipcRenderer.send('save-dialog', {
    type: 'config'
    title: 'Export your config',
    filters: [{ name: 'JSON file', extensions: ['json'] }]
    defaultPath: "Vocabus-config.json"
  })
  return false

ipcRenderer.on 'save-dialog-response', (event, msg) ->
  return unless msg.type == 'config' and msg.path?
  mappedConfig = {}
  for key, value of config
    mappedConfig[key] = value.get()
  fs.writeFile msg.path, JSON.stringify(mappedConfig), (err) ->
    alert if err then 'Unexpected error' else 'Config exported'

$body.on 'click', 'a[target=config-import]', ->
  ipcRenderer.send('open-dialog', {
    type: 'config'
    title: 'Import a config',
    filters: [{ name: 'JSON file', extensions: ['json'] }]
    properties: ['openFile']
  })
  return false

ipcRenderer.on 'open-dialog-response', (event, msg) ->
  return unless msg.type == 'config' and msg.path?
  fs.readFile msg.path[0], 'utf-8', (err, data) ->
    try
      throw 'Unexpected error' if err
      data = JSON.parse data
      throw 'Invalid file structure' unless typeof data == 'object'
      loaded = []
      for key, value of config
        if data[key]?
          config[key].set(data[key])
          loaded.push key
      throw 'Invalid file structure, no entries loaded' unless loaded.length
      alert "Configuration imported (entries: #{loaded.join(', ')})"
      window.location.reload()
    catch err
      alert 'Cannot load the configuration file'
