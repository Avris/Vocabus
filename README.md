## Vocabus - Dictionary at your fingertips!##

Whatever you're doing on your computer, this dictionary is always just two hotkeys away!
In a browser, document, pdf reader, everywhere!

Just select a word you want to look up and press `Ctrl+C` to put it in the Clipboard:

Now just press `Ctrl+Shift+C`.
Vocabus will show up and look up a word for you! It's quick and comfortable, isn't it?

Use `Ctrl+Shift+X` to just invoke Vocabus, without pasting anything to the search box.

Switch between your favourite dictionaries with `Up` and `Down` keys.

Every translation can also be saved in your personal list of words to repeat – just click on a plus icon.

### Copyright ###

* **Author:**  Andrzej Prusinowski [(Avris.it)](https://avris.it)
* **License:** [MIT](https://opensource.org/licenses/MIT)
* **Engine:** [Electron](http://electron.atom.io/)
* **Dictionary API**: [PONS](https://pons.com/)
